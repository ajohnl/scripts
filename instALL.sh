#!/bin/bash
mkdir -p ~/.vim/autoload ~/.vim/bundle
echo "Insert 1 to install i3 configs"
echo "Insert 2 to install vim configs"
echo "Insert 3 to install syntastic for vim and vim configs"
echo "Insert 4 to extract vim files into your home"
while true; do
	    read -p "Make your decision, young one." option
		    case $option in
				        [1]* ) cp i3status ~/.i3status.conf;mkdir -p ~/.config/i3;cp i3config ~/.config/i3/config; break;;
						[2]* ) cp vimrc ~/.vimrc; git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim; vim +PluginInstall +qall; break;;
						[3]* ) cp vimrc ~/.vimrc; curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim; cd ~/.vim/bundle; git clone --depth=1 https://github.com/vim-syntastic/syntastic.git; cd -; break;;
						[4]* ) cp vim.tar.gz ~/vim.tar.gz ; cd /~ ; tar xfvz vim.tar.gz ; cd -; break;;
						* ) echo "Please answer!!!";;
			esac
done
echo "Have a nice day."

