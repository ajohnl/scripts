syntax on
set backspace=indent,eol,start
set secure
set exrc
set nocompatible          " Only iMproved
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
let g:load_doxygen_syntax=1
"Plugin 'vim-scripts/c.vim'
Plugin 'vim-scripts/Conque-GDB'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-fugitive'
Plugin 'wincent/command-t'
Plugin 'scrooloose/nerdcommenter'
Plugin 'vhdirk/vim-cmake'
Plugin 'teneighty/vim-ant'
Plugin 'vim-scripts/Trailer-Trash'
Plugin 'vim-scripts/bad-whitespace'
Plugin 'vim-scripts/html-xml-tag-matcher'
Plugin 'vim-syntastic/syntastic'
Plugin 'danro/rename.vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'ervandew/supertab'
Plugin 'ciaranm/detectindent'
Plugin 'altercation/vim-colors-solarized'
Plugin 'lervag/vimtex'
Plugin 'lervag/vim-latex'
Plugin 'vim-scripts/DoxygenToolkit.vim'
Plugin 'maciakl/vim-neatstatus'
Plugin 'scrooloose/nerdtree'
Plugin 'VundleVim/Vundle.vim'
Plugin 'Townk/vim-autoclose'
"Plugin 'coot/atp_vim'
let g:syntastic_cpp_compiler_options = ' -std=c++11 '
call vundle#end()
filetype plugin indent on
set nocp
filetype plugin on
set background=dark
colorscheme solarized
" General
set tabstop=4
set shiftwidth=4
set noexpandtab
syntax enable
set number
set autoindent
set wildmenu
set showmatch
set incsearch
set hlsearch
set incsearch nohlsearch
set wrap
set linebreak
set nolist

nmap j gj
nmap k gk

nmap <S-J> :tabp<CR>
nmap <S-K> :tabn<CR>

noremap <leader>m <Esc>:CommandTFlush<CR>
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let  g:C_UseTool_cmake = 'yes'
let  g:C_UseTool_doxygen = 'yes'
